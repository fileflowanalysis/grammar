plugins {
    antlr
}

group = "edu.utsa.fileflow"
version = "1.0"

repositories {
    mavenCentral()
}

dependencies {
    antlr("org.antlr:antlr4:4.5")
}